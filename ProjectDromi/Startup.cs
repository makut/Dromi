﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjectDromi.Models;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;


namespace ProjectDromi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DromiContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("DromiContext")));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc(o => o.InputFormatters.Insert(0, new RawRequestBodyFormatter()))
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    };
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                    options.SerializerSettings.Formatting = Formatting.Indented;
                    // options.InputFormatters
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Dromi API",
                    Description = "API for Dromi Services",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Dorcas Maku Tamatey",
                        Email = "dorcas@hubtel.com",
                        Url = "https://twitter.com/mtamatey"
                    },
                    License = new License
                    {
                        Name = "None"

                    },

                });
                c.IncludeXmlComments(string.Format(@"{0}ProjectDromi.xml",
                         AppDomain.CurrentDomain.BaseDirectory));
                // var filePath = Path.Combine(System.AppContext.BaseDirectory, "Dromi.xml");
                // c.IncludeXmlComments(filePath);

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dromi API V1");
            });


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

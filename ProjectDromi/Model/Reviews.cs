﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDromi.Models
{
    public class Reviews
    {
        public int ReviewsId { get; set; }
        public string Comments { get; set; }
        public int NumStars { get; set; }
        public int ProfileId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDromi.Models
{
    public class Gallery
    {
        public int GalleryId { set; get; }
        public string ImageName { set; get; }
        public string ImageDescription { set; get; }
        public int ProfileId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDromi.Models
{
    public class Profile
    {
        public int ProfileId { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Contact { get; set; }
        public string Availabitity { get; set; }
        public string image { get; set; }
        public int AvailableCategoriesId { get; set; }

        public ICollection<Reviews> Reviews { get; set; }
        public ICollection<Gallery> Images { get; set; }



    }

}
